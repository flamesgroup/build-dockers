Collection of Dockerfiles for building each part of ANTRAX software

## Overview

The aim of this project is to collect build environments for all parts of ANTRAX system. It may serve as starting point in setting gitlab-runner over Docker.

## Directory structure

```
build-dockers/                           #
|----ubuntu-14.04-crosscompile/          #
|    |----Dockerfile                     # Ubuntu 14.04 for ARM Allwinner A20 compilation
|----ubuntu-14.04-rootfs                 #
|    |----Dockerfile                     # Ubuntu 14.04 for Debian image bild
|    |----aget.py                        # Allow to download artifacts from GitLab
|----ubuntu-16.04-jdk/                   #
|    |----Dockerfile                     # Ubuntu 16.04 with all necessary to build ANTRAX Java components
|    |----config                         # SSH config files to access debian and centos repositories. All sensitive data removed
|    |----gradle.properties              # Gradle and Maven properties may contain sensitive data so only stubs presented here
|    |----id_rsa                         #
|    |----id_rsa.pub                     #
|    |----settings.xml                   #
|----ubuntu-16.04-sunxi-tools/           #
|    |----Dockerfile                     # Ubuntu 16.04 with sunxi-tools
|----LICENSE                             # Project licensed under GPLv3 license
|----README.md                           #
```

## Usage

Images provided by this project may be used in conjunction with GitLab container register, nevertheless this project does not provide ready-to-use container register because of sensitive data that may be present in built images. To use this images just build them with docker on machine with GitLab runner which will be used to build ANTRAX components.
